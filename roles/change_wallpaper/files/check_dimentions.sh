#!/usr/bin/env bash

IMG=$1

# Check if the image is downloaded correctly
if [[ ! `identify $1` ]]; then
  echo "bad file"
  exit 1
fi

# Get image's height
IMG_H=`identify -format '%h' $1`

# Get image's width
IMG_W=`identify -format '%w' $1`

if [[ $IMG_W > $IMG_H ]]; then
  echo "landscape"
else
  echo "portait"
fi
